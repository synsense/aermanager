.. aermanager documentation master file, created by
   sphinx-quickstart on Tue Nov 24 15:17:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aermanager's documentation!
======================================


This package allows users to work and manipulate event data recorded from various event based sensors.
In particular, this package is extremely useful for pre-processing and generating datasets to work with `sinabs <https://sinabs.io>`_.

You can install this package with pip.

.. code-block:: bash

   $ pip install aermanager

Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   tutorials
   notebooks/How_to
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
