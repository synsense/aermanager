API
===

.. automodule:: aermanager
    :members:

aerparser
---------

.. automodule:: aermanager.aerparser
    :members:

parsers
-------

.. automodule:: aermanager.parsers
    :members:

annotate_aedat
--------------

.. automodule:: aermanager.annotate_aedat
    :members:

dataset_generator
-----------------

.. automodule:: aermanager.dataset_generator
    :members:

datasets
--------

.. autoclass:: aermanager.datasets.HDF5FolderDataset
    :members:

.. autoclass:: aermanager.datasets.FramesDataset
    :members:
    :show-inheritance:

.. autoclass:: aermanager.datasets.SpikeTrainDataset
    :members:
    :show-inheritance:

liveaer
-------

.. automodule:: aermanager.liveaer
    :members:

preprocess
----------

.. automodule:: aermanager.preprocess
    :members:

transform
---------

.. automodule:: aermanager.transform
    :members:

visualization
-------------

.. automodule:: aermanager.visualization
    :members:
